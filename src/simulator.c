#include "simulator.h"
#include "helper.h"
#include "archi.h"
#include "instructions.h"
#include <string.h>
#include <limits.h>

#define MAX_LEN (128)

void start()
{
	regs_init();
	ports_init();
	instructions_init();
}

void reset()
{
	for (int i = 0; i < N_REGS; i++) {
		if (regs[i] != NULL) {
			regs[i]->val = 0;
		}
	}
	ports_init();
}
