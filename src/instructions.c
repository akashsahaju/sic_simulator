#include "instructions.h"
#include "archi.h"
#include <stdio.h>

void (*instructions[1 << 8]) (int addr);

void instructions_init()
{
	instructions[0x00] = lda;
	instructions[0x50] = ldch;
	instructions[0x08] = ldl;
	instructions[0x04] = ldx;
	instructions[0x0C] = sta;
	instructions[0x54] = stch;
	instructions[0x14] = stl;
	instructions[0x10] = stx;
	instructions[0x18] = add;
	instructions[0x1C] = sub;
	instructions[0x24] = div_asm;
	instructions[0x20] = mul;
	instructions[0x40] = and_asm;
	instructions[0x44] = or_asm;
	instructions[0x28] = comp;
	instructions[0x3C] = j;
	instructions[0x30] = jeq;
	instructions[0x34] = jgt;
	instructions[0x38] = jlt;
	instructions[0x48] = jsub;
	instructions[0x4C] = rsub;
	instructions[0xFD] = hlt;
	instructions[0x2C] = tix;
	instructions[0xE0] = td;
	instructions[0xD8] = rd;
	instructions[0xDC] = wd;
}

//implementing lda : load to accumulator
void lda(int address){  
    a.val=view_word(address);
}

//implementing ldch : A[rightmost 8 bit]->m
void ldch(int address){
		int word = mem[address];
        int tmpch = (int)word;
        tmpch = tmpch & 0xFF;
        a.val= a.val & 0xFFFFFF00;
        a.val = a.val | tmpch;
	
}

//implementing ldl load to L register
void ldl(int address){  
    l.val=view_word(address);
}

//implementing ldx load to X register
void ldx(int address){  
    x.val=view_word(address);
}
//implementing sta : store from accumulator
void sta(int address){
	write_word(address,a.val);
}

//implement stch m->A[rightmost 8 bit]
void stch(int address){
	int tmp=a.val & 0xFF;
	mem[address]=tmp;
}

//implement stl : store L register
void stl(int address){
	write_word(address,l.val);
}

//implement stx : store X register
void stx(int address){
	write_word(address,x.val);
}


//implementing ADD
void add(int address){
	int word=view_word(address);
	a.val=a.val+word;
}

//implementing SUB : Subtraction
void sub(int address){
	int word=view_word(address);
	a.val=a.val-word;
}

//implementing DIV : division
void div_asm(int address){
	int word=view_word(address);
	if(!word){
        printf("Divide by Zero Error\n");
		return;
	}
	a.val=a.val/word;
}

//implementing MUL : Multiplication
void mul(int address){
	int word=view_word(address);
	a.val=a.val*word;
}

//implementing AND

void and_asm(int address){
	int word=view_word(address);
	a.val=a.val & word;
}

//implementing OR

void or_asm(int address){
	int word=view_word(address);
	a.val=a.val | word;
}

//comparator function
void comp(int address){
	int word=view_word(address);
	if(a.val==word) sw.val=0;
	else if(a.val<word)sw.val=-1;
	else sw.val=1;
	 /***
        CC = 0 -> Equal
        CC = -1 -> Less than
        CC = 1 -> Greater than
        ***/
}

//implementing j LABEL : jump
void j(int address){
	pc.val=address;
}

//implement jeq : jump if equal
void jeq(int address){
	if(sw.val==0)
	pc.val=address;
}

//implement jump on greater than jgt
void jgt(int address){
	if(sw.val==1)
	pc.val=address;
}

//implement JLT jump on lower than
void jlt(int address){
	if(sw.val==-1)
	pc.val=address;
}

//implement jsub
void jsub(int address){
	l.val=pc.val;
	pc.val=address;
}

//implement rsub
void rsub(int address){
	(void)address;
	pc.val=l.val;
}

void hlt(int address)
{
	(void)address;
	return;
}

//implementing TIX
void tix(int address){
	//increase index register by 1
	x.val=x.val+1;
	int word=view_word(address);
	if(x.val==word) sw.val=0;
	else if(x.val<word)sw.val=-1;
	else x.val=1;
	 /***
        CC = 0 -> Equal
        CC = -1 -> Less than
        CC = 1 -> Greater than
        ***/
	
}

void td(int address)
{
	int port_no = mem[address] & 0xFF;
	if (port_no < N_INPUT_PORTS && is_port_empty(&ports[port_no])) {
		sw.val = 0;
	} else if (port_no >= N_INPUT_PORTS && is_port_full(&ports[port_no])) {
		sw.val = 0;
	} else {
		sw.val = 1;
	}
}

void rd(int address)
{
	int port_no = mem[address] & 0xFF;
	int word = ports[port_no].buf[ports[port_no].r_ptr];
	ports[port_no].r_ptr = (ports[port_no].r_ptr + 1) % MAX_BUF;
	int tmpch = (int)word;
	tmpch = tmpch & 0xFF;
	a.val= a.val & 0xFFFFFF00;
	a.val = a.val | tmpch;
}

void wd(int address)
{
	int port_no = mem[address] & 0xFF;
	int word = a.val & 0xff;
	ports[port_no].buf[ports[port_no].w_ptr] = word & 0xff;
	ports[port_no].w_ptr = (ports[port_no].w_ptr + 1) % MAX_BUF;
}
