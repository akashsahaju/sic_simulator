#include "assembler.h"
#include "simulator.h"
#include "runner.h"
#include "loader.h"
#include <stdlib.h>
#include <string.h>
#include <gtk/gtk.h>

GtkWidget *status_label; //to display status each time button is clicked
GtkWidget *output_label; //to display output
GtkWidget *convert_label;//to display converted number
 
/*to display register contents*/
GtkWidget *label_A;
GtkWidget *label_X;
GtkWidget *label_L;
GtkWidget *label_PC;
GtkWidget *label_SW;

const char fileSelected[300] ;//stores the file selected
const char textEntered[300];//stores the text entered into entry section
const char numberEntered[300];//stores the text entered into entry2 section





static void destroy(GtkWidget *widget, gpointer data) 
{
    gtk_main_quit();
}

static void hex_to_decCall()
{

  char result[100];

  int num = strtol(numberEntered, NULL, 16);
  sprintf(result, "%d", num);
  //result=hex_to_dec(result,numberEntered);
  gtk_label_set_text(GTK_LABEL(convert_label),(const char*)&result);
  result[0]='\0';
}
static void dec_to_hexCall()
{
   char result[100];

   int num = strtol(numberEntered, NULL, 10);
   sprintf(result, "%X", num);
  //result=dec_to_hex(result,numberEntered);
  gtk_label_set_text(GTK_LABEL(convert_label),(const char*)&result);
  result[0]='\0';
}
  
static void setRegister() //to set register values
{
  char buffer[100];

  int val=view_reg("A");
  sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_A),(const char*)&buffer);
  buffer[0]='\0';

  val=view_reg("X");
  sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_X),(const char*)&buffer);
  buffer[0]='\0';

   val=view_reg("L");
   sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_L),(const char*)&buffer);
  buffer[0]='\0';

   val=view_reg("PC");
  sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_PC),(const char*)&buffer);
  buffer[0]='\0';

   val=view_reg("SW");
   sprintf(buffer,"%06X",val);
  gtk_label_set_text(GTK_LABEL(label_SW),(const char*)&buffer);
  buffer[0]='\0';
}
static void assemblerCall()      //function which ASSEMBLE button calls
{
  int len=strlen(fileSelected);
  char file[len+1];
  strcpy(file,fileSelected);
  FILE *input_file = fopen(file, "r");
  FILE *output_file = fopen("../output/obj.txt", "w+");
  assemble(input_file, output_file);
  fclose(input_file);
  fclose(output_file);
}
static void loaderCall()          //function which LOAD button calls
{
  int len=strlen(fileSelected);
  char file[len+1];
  strcpy(file,fileSelected);
  load(file);
  setRegister();
}
static void runnerCall()           //function which RUN button calls
{
   exec_prog();
   setRegister();
}
static void execNextCall()                            //function which EXEC_NEXT button calls
{
  int ret=exec_next();
  if(ret==EXIT_PROG)
  gtk_label_set_text(GTK_LABEL(status_label),"HALT");
  else gtk_label_set_text(GTK_LABEL(status_label),"RAN");
  setRegister();
}
static void memoryCall()                                //function which VIEW MEMORY button calls
{
  int addr=strtol(textEntered,NULL,0);
  int content=view_mem(addr);

  char buffer[100];
  sprintf(buffer,"%02X",content);
  gtk_label_set_text(GTK_LABEL(output_label),(const char*)&buffer);
  buffer[0]='\0';

  setRegister();
}
static void wordCall()                                    //function which VIEW WORD button calls
{
  int addr=strtol(textEntered,NULL,0);
  int content=view_word(addr);

  char buffer[100];
  sprintf(buffer,"%06X",content);
  gtk_label_set_text(GTK_LABEL(output_label),(const char*)&buffer);
  buffer[0]='\0';
}

static void resetCall()                                   //function which RESET button calls
{
	reset();
	setRegister();
	gtk_label_set_text(GTK_LABEL(status_label),"Welcome!");
	// gtk_label_set_text(GTK_LABEL(output_label),"-");
}

static void file_selected(GtkWidget *filechooserbutton, gpointer data)
{
    const char* file=gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooserbutton));
    int len=strlen(file);
  strcpy(fileSelected,file);
   // g_print("Selected file: %s", gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(filechooserbutton)));
     g_print("Selected file: %s", fileSelected);
}

static void entry_activated(GtkWidget *entry, gpointer data)
{
    const char* text=gtk_entry_get_text(GTK_ENTRY(entry));

  strcpy(textEntered,text);
    g_print("Entry text: '%s'\n", gtk_entry_get_text(GTK_ENTRY(entry)));
}

static void entry_conv_activated(GtkWidget *entry_conv, gpointer data)
{
    const char* text=gtk_entry_get_text(GTK_ENTRY(entry_conv));
  
  strcpy(numberEntered,text);
    g_print("Entry text: '%s'\n", numberEntered);
}

static void print_message1 (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Code assembled!");
  g_print ("Code assembled!!\n");
}

static void print_message2 (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Code loaded!");
  g_print ("Code loaded!!\n");
}

static void print_message3 (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Code ran!");
  g_print ("Code ran!!\n");
}

static void print_message4 (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Showing memory content!");
  g_print ("Showing memory content!!\n");
}
static void print_message5 (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Register content reset!");
  g_print ("Register content reset!!\n");
}
static void print_message6 (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Showing word!");
  g_print ("Showing word!!\n");
}
/*static void print_messageA (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Showing content of 'a' register!!");
  g_print ("Showing content of 'a' register!!\n");
}


static void print_messageX (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Showing content of 'x' register!!");
  g_print ("Showing content of 'x' register!!\n");
}
static void print_messageL (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Showing content of 'l' register!!");
  g_print ("Showing content of 'l' register!!\n");
}
static void print_messagePC (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Showing content of 'pc' register!!");
  g_print ("Showing content of 'pc' register!!\n");
}
static void print_messageSW (GtkWidget *widget,
             gpointer   data)
{
  gtk_label_set_text(GTK_LABEL(status_label),"Showing content of 'sw' register!!");
  g_print ("Showing content of 'sw' register!!\n");
}*/

int main(int argc, char *argv[])
{
	start();//simulator started;

	gtk_init(&argc, &argv);

	GtkWidget *window;             //The entire box
	GtkWidget *button;             //performs actions on being clicked
								   //action specified in corresponding 
								   //g_signal_connect() function

	GtkWidget *label;              //to display text which is set into it 
	GtkWidget *grid;               //for packing all the widgets
	GtkWidget *entry;              //takes input from user
 	GtkWidget *entry_conv;         //takes input to be converted
	GtkWidget *filechooserbutton;  //provides list of files to select from



	/*setting up the window*/
	window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(window), "SIC Simulator");
	gtk_window_set_default_size(GTK_WINDOW(window), 500, 330);
	g_signal_connect(window, "destroy", G_CALLBACK(destroy), NULL);

	/*setting up the grid*/
	grid=gtk_grid_new();
	gtk_container_add(GTK_CONTAINER(window),grid);



	/*THis field takes an input,on pressing enter,the input appears on terminal*/
	/*To fetch this input, see the entry_activated() function above*/
	entry=gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(entry), "");
	gtk_entry_set_placeholder_text(GTK_ENTRY(entry), "Type address here");
	g_signal_connect(GTK_ENTRY(entry), "activate", G_CALLBACK(entry_activated), NULL);
        gtk_widget_set_tooltip_text(GTK_WIDGET(entry), "Enter address here");
	gtk_grid_attach (GTK_GRID (grid), entry, 160, 222, 120, 10);  //x,y,width,height

	/*to convert from one base to another*/
        entry_conv=gtk_entry_new();
        gtk_entry_set_text(GTK_ENTRY(entry_conv), "");
	gtk_entry_set_placeholder_text(GTK_ENTRY(entry_conv), "Type number here");
	g_signal_connect(GTK_ENTRY(entry_conv), "activate", G_CALLBACK(entry_conv_activated), NULL);
        gtk_widget_set_tooltip_text(GTK_WIDGET(entry_conv), "Enter number here");
	gtk_grid_attach (GTK_GRID (grid), entry_conv, 67, 0, 10, 10);  //x,y,width,height



	status_label = gtk_label_new("Welcome!");                 //default text is status box,font can be modified
        gtk_widget_set_tooltip_text(GTK_WIDGET(status_label), "Displaying current status of code");
	gtk_grid_attach(GTK_GRID(grid), status_label,65,165,67,10); //label attched into the main grid
	//gtk_label_set_pattern(GTK_LABEL(status_label),"______________________________________________________________________________");


	convert_label = gtk_label_new("output");                 //default text is output box,font can be modified
        gtk_widget_set_tooltip_text(GTK_WIDGET(convert_label), "Displays converted number");
	gtk_grid_attach(GTK_GRID(grid), convert_label,80,0,20,10); //label attched into the main grid

	output_label = gtk_label_new("Output");                 //default text is output box,font can be modified
        gtk_widget_set_tooltip_text(GTK_WIDGET(output_label), "Displays output");
	gtk_grid_attach(GTK_GRID(grid), output_label,167,165,67,10); //label attched into the main grid

	/*label=gtk_label_new("OUTPUT");                              //OUTPUT label/heading
	gtk_grid_attach(GTK_GRID(grid),label,167,152,67,10);
	gtk_label_set_pattern(GTK_LABEL(label),"__________");*/



	label = gtk_label_new("Registers");                 //heading is registers,font can be modified
	gtk_grid_attach(GTK_GRID(grid), label,175,0,70,10); //label attched into the main grid
        gtk_label_set_pattern(GTK_LABEL(label),"__________");

	label = gtk_label_new("Contents");                 //heading is contents,font can be modified
	gtk_grid_attach(GTK_GRID(grid), label,232,0,70,10); //label attched into the main grid
        gtk_label_set_pattern(GTK_LABEL(label),"__________");


	/*Vertically placed one by one,all the five register labels of SIC*/
	label = gtk_label_new("A");
	gtk_grid_attach (GTK_GRID (grid), label, 190, 10, 30, 10);

	label = gtk_label_new("X");
	gtk_grid_attach (GTK_GRID (grid), label, 190, 20, 30, 10);

	label = gtk_label_new("L");
	gtk_grid_attach (GTK_GRID (grid), label, 190, 30, 30, 5);

	 label = gtk_label_new("PC");
	gtk_grid_attach (GTK_GRID (grid), label, 190, 35, 30, 5);

	label = gtk_label_new("SW");
	gtk_grid_attach (GTK_GRID (grid), label, 190, 40, 30, 5);


	/*Vertically placed one by one ,labels to display register contents after successful execution of program*/
	label_A = gtk_label_new("000000");
	gtk_grid_attach (GTK_GRID (grid), label_A, 250, 10, 30, 10);

	label_X = gtk_label_new("000000");
	gtk_grid_attach (GTK_GRID (grid), label_X, 250, 20, 30, 10);

	label_L = gtk_label_new("000000");
	gtk_grid_attach (GTK_GRID (grid), label_L, 250, 30, 30, 5);

	label_PC = gtk_label_new("000000");
	gtk_grid_attach (GTK_GRID (grid), label_PC, 250, 35, 30, 5);

	label_SW = gtk_label_new("000000");
	gtk_grid_attach (GTK_GRID (grid), label_SW, 250, 40, 30, 5);


	/*for hex to dec conversion*/
        button = gtk_button_new_with_label ("Hex_to_Dec");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Converts number from hexadecimal to decimal");
	//g_signal_connect (button, "clicked", G_CALLBACK (print_message1), NULL);
	g_signal_connect (button, "clicked", G_CALLBACK (hex_to_decCall), NULL);
	gtk_grid_attach (GTK_GRID (grid), button, 67, 10, 5, 2);

	/*for decimal to hexadecimal conversion*/
	button = gtk_button_new_with_label ("Dec_to_Hex");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Converts number from decimal to hexadecimal");
	//g_signal_connect (button, "clicked", G_CALLBACK (print_message1), NULL);
	g_signal_connect (button, "clicked", G_CALLBACK (dec_to_hexCall), NULL);
	gtk_grid_attach (GTK_GRID (grid), button, 74, 10, 5, 2);

	
	/*SEtting up the buttons for basic operations*/
	button = gtk_button_new_with_label ("Run"); 
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Execution starts on single click");
	g_signal_connect (button, "clicked", G_CALLBACK (print_message3), NULL);//G_CALLBACK() is the function that takes the function to be executed as parameter
	g_signal_connect (button, "clicked", G_CALLBACK (runnerCall), NULL);
	gtk_grid_attach (GTK_GRID (grid), button, 60, 162, 2, 2);

	/*For assembling*/
	button = gtk_button_new_with_label ("Assemble");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Assembling starts on single click");
	g_signal_connect (button, "clicked", G_CALLBACK (print_message1), NULL);
	g_signal_connect (button, "clicked", G_CALLBACK (assemblerCall), NULL);
	gtk_grid_attach (GTK_GRID (grid), button, 60, 160, 1, 2);

	/*for loading*/
	button = gtk_button_new_with_label ("Load");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Loading starts on single click");
	g_signal_connect (button, "clicked", G_CALLBACK (print_message2), NULL);
	g_signal_connect (button, "clicked", G_CALLBACK (loaderCall), NULL);
	gtk_grid_attach (GTK_GRID (grid), button, 61, 160, 1, 2);

	/*to view memory contents*/
	button = gtk_button_new_with_label ("View memory");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Shows content of specified memory location");
	g_signal_connect (button, "clicked", G_CALLBACK (print_message4), NULL);
	g_signal_connect (button, "clicked", G_CALLBACK (memoryCall), NULL);
	gtk_grid_attach (GTK_GRID (grid), button, 160, 232, 55, 10);

	/*to view word*/
	button = gtk_button_new_with_label ("View word");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Shows word on single click");
	g_signal_connect (button, "clicked", G_CALLBACK (print_message6), NULL);
	g_signal_connect (button, "clicked", G_CALLBACK (wordCall), NULL);
	gtk_grid_attach (GTK_GRID (grid), button, 220, 232, 65, 10);

	/*to execute next instruction*/
	button = gtk_button_new_with_label ("Next");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Executes next instruction on single click");
	//g_signal_connect(button, "clicked", G_CALLBACK (print_message5), window);
	g_signal_connect(button, "clicked", G_CALLBACK (execNextCall), window);
	gtk_grid_attach (GTK_GRID (grid), button,60,196,3,2);


	 /*to reset register values*/
	button = gtk_button_new_with_label ("Reset");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Resets register values on single click");
	g_signal_connect(button, "clicked", G_CALLBACK (print_message5), window);
	g_signal_connect(button, "clicked", G_CALLBACK (resetCall), window);
	gtk_grid_attach (GTK_GRID (grid), button,60,219,3,2);


	/*to exit*/
	button = gtk_button_new_with_label ("Quit");
        gtk_widget_set_tooltip_text(GTK_WIDGET(button), "Exits on single click");
	//g_signal_connect(button, "clicked", G_CALLBACK (print_message5), window);
	g_signal_connect_swapped(button, "clicked", G_CALLBACK (gtk_widget_destroy), window);
	gtk_grid_attach (GTK_GRID (grid), button,60,232,3,2);


	/*to choose files from list.to fetch the selected file,see file_selected function defined above*/
	filechooserbutton = gtk_file_chooser_button_new("Select file",
																	  GTK_FILE_CHOOSER_ACTION_OPEN);
        gtk_widget_set_tooltip_text(GTK_WIDGET(filechooserbutton), "Select file");
	g_signal_connect(filechooserbutton, "file-set", G_CALLBACK(file_selected), NULL);
	gtk_grid_attach(GTK_GRID(grid),filechooserbutton,0,0,66,10);


	 //label = gtk_label_new("hello");
	 //gtk_label_set_text(GTK_LABEL(label),"code assembled");
	 //gtk_grid_attach(GTK_GRID(grid), label,10,10,70,10);

	//const char *text = gtk_label_get_text(GTK_LABEL(label));
	//printf("%s\n", text);
	//const char* input=gtk_entry_get_text(GTK_ENTRY(entry));
	//printf("%s\n",input);

	gtk_widget_show_all(window);

	gtk_main();

	return 0;
}
