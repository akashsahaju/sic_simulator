#include "archi.h"
#include "helper.h"
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <stdbool.h>

#define MAX_LEN 256

int mem[MEM_SIZE];

bool is_port_empty(const Port *port)
{
	return port->r_ptr == port->w_ptr;
}

bool is_port_full(const Port *port)
{
	return (port->r_ptr + 1) % MAX_BUF == port->w_ptr;
}

#define N_PORTS 10

Port ports[N_PORTS];

void ports_init()
{
	for (int i = 0; i < N_PORTS; i++) {
		ports[i].r_ptr = 0;
		ports[i].w_ptr = 0;
		ports[i].buf[0] = '\0';
	}
}

void enter_str_to_port(int port_no, const char *str)
{
	Port *port = &ports[port_no];
	const char *c = str;
	while (*c != '\0') {
		port->buf[port->w_ptr] = *c;
		port->w_ptr = (port->w_ptr + 1) % MAX_BUF;
		c++;
	}
	port->w_ptr = '\0';
	port->w_ptr = (port->w_ptr + 1) % MAX_BUF;
}

char *read_str_from_port(char *str, int port_no)
{
	Port *port = &ports[port_no];
	while (true) {
		*str = port->buf[port->r_ptr];
		str++;
		port->r_ptr = (port->r_ptr + 1) % MAX_BUF; 
		if (*(str - 1) == '\0') {
			break;
		}
	}
	return str;
}

int view_mem(int addr)
{
	return mem[addr] & 0xff;
}

void write_mem(int addr, int val)
{
	mem[addr] = val & 0xff;
}

// read a 24 bit word from memory 
int view_word(int address)
{
	int num1 = 0,num2 = 0,i;

	for( i = 0; i < 3; ++i)
	{
		int ch = mem[address++];
		num2 = (int)ch;
		num2 = num2 & 0x000000FF;
		num1 = num1 | num2;
		num1 = num1 << 8;
	}
    
    // making the numbers behave as signed 24 bit 
	// if MSB as 1 maintain as 2's complement
    num1 = num1 >> 8;
    if(num1 & 0x800000)
    {
        num1 = num1 | 0xFF000000;
    }
	return num1;	
}

// store a 24 bit word to memory 
void write_word(int address, int data)
{
	int ch;
	int i;
    for( i = 2; i >= 0; i--)
    {
        ch = data & 0xFF;
        mem[address + i] = ch;
        data = data >> 8;
    }
}

int view_reg(char const *reg_name)
{
	char reg_name_upper[MAX_LEN + 1] = "";
	strcpy(reg_name_upper, reg_name);
	strtoupper(reg_name_upper);

	if (!strcmp(reg_name_upper, "A")) {
		return a.val & 0xffffff;
	} else if (!strcmp(reg_name_upper, "X")) {
		return x.val & 0xffffff;
	} else if (!strcmp(reg_name_upper, "L")) {
		return l.val & 0xffffff;
	} else if (!strcmp(reg_name_upper, "PC")) {
		return pc.val & 0xffffff;
	} else if (!strcmp(reg_name_upper, "SW")) {
		return sw.val & 0xffffff;
	}

	return INT_MIN;
}

Reg a = { "A", 0, 0 };
Reg x = { "X", 1, 0 };
Reg l = { "L", 2, 0 };
Reg pc = { "PC", 8, 0 };
Reg sw = { "SW", 9, 0 };

Reg *regs[N_REGS] = { NULL };

void regs_init()
{
	regs[a.no] = &a;
	regs[x.no] = &x;
	regs[l.no] = &l;
	regs[pc.no] = &pc;
	regs[sw.no] = &sw;
}
