#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include <stdio.h>
#include <stdbool.h>

// returns true if assembling was successful
bool assemble(const char *input_file_path, const char *output_file_path);

#endif
