#ifndef HELPER_H
#define HELPER_H

char *strrev(char *str);

char *strtoupper(char *str);

char *strtok_single(char *str, char const *delims);

char *hex_to_dec(char *res, char *input);

char *dec_to_hex(char *res, char *input);

#endif
