CC = gcc
CFLAGS = -g -Wall -std=c99
GTK_COM_FLAGS = `pkg-config --cflags gtk+-3.0` \
				`pkg-config --cflags gtksourceview-3.0`
GTK_LINK_FLAGS = `pkg-config --libs gtk+-3.0` \
				 `pkg-config --libs gtksourceview-3.0` -export-dynamic

SRC_DIRECTORY = src
default: simulator_gui

executable = simulator

simulator_com_objects = strmap.o helper.o assembler.o loader.o archi.o\
					instructions.o runner.o simulator.o

simulator_cli_objects = cli.o

simulator_gui_objects = gui.o

simulator_gui: $(simulator_com_objects) $(simulator_gui_objects)
	$(CC) $(CFLAGS) -o $(executable) $^ $(GTK_LINK_FLAGS)

simulator_cli: $(simulator_com_objects) $(simulator_cli_objects)
	$(CC) $(CFLAGS) -o $(executable) $^

gui.o: $(SRC_DIRECTORY)/gui.c $(SRC_DIRECTORY)/assembler.h $(SRC_DIRECTORY)/simulator.h $(SRC_DIRECTORY)/runner.h $(SRC_DIRECTORY)/loader.h

$(simulator_gui_objects):
	$(CC) $(CFLAGS) -c $< $(GTK_COM_FLAGS)

assembler_test: strmap.o helper.o assembler.o assembler_test.o
	$(CC) $(CFLAGS) -o $@ $^

strmap.o: $(SRC_DIRECTORY)/strmap.c $(SRC_DIRECTORY)/strmap.h
	$(CC) $(CFLAGS) -c $<

helper.o: $(SRC_DIRECTORY)/helper.c $(SRC_DIRECTORY)/helper.h
	$(CC) $(CFLAGS) -c $<

assembler.o: $(SRC_DIRECTORY)/assembler.c $(SRC_DIRECTORY)/assembler.h $(SRC_DIRECTORY)/strmap.h $(SRC_DIRECTORY)/helper.h
	$(CC) $(CFLAGS) -c $<

archi.o: $(SRC_DIRECTORY)/archi.c $(SRC_DIRECTORY)/archi.h
	$(CC) $(CFLAGS) -c $<

loader.o: $(SRC_DIRECTORY)/loader.c $(SRC_DIRECTORY)/loader.h $(SRC_DIRECTORY)/archi.h
	$(CC) $(CFLAGS) -c $<

instructions.o: $(SRC_DIRECTORY)/instructions.c $(SRC_DIRECTORY)/instructions.h $(SRC_DIRECTORY)/archi.h
	$(CC) $(CFLAGS) -c $<

runner.o: $(SRC_DIRECTORY)/runner.c $(SRC_DIRECTORY)/runner.h $(SRC_DIRECTORY)/archi.h $(SRC_DIRECTORY)/instructions.h
	$(CC) $(CFLAGS) -c $<

simulator.o: $(SRC_DIRECTORY)/simulator.c $(SRC_DIRECTORY)/simulator.h $(SRC_DIRECTORY)/helper.h $(SRC_DIRECTORY)/archi.h $(SRC_DIRECTORY)/instructions.h
	$(CC) $(CFLAGS) -c $<

cli.o: $(SRC_DIRECTORY)/cli.c $(SRC_DIRECTORY)/simulator.h $(SRC_DIRECTORY)/runner.h $(SRC_DIRECTORY)/loader.h $(SRC_DIRECTORY)/assembler.h
	$(CC) $(CFLAGS) -c $<

assembler_test.o: assembler_test.c $(SRC_DIRECTORY)/assembler.h $(SRC_DIRECTORY)/helper.h
	$(CC) $(CFLAGS) -c $<

.PHONY: clean_assembler_test

.PHONY: clean

clean_assembler_test:
	rm assembler_test *.o

clean:
	rm $(executable) *.o
